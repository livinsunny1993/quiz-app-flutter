import 'package:flutter/material.dart';

import 'package:myapp/start_screen.dart';
import 'package:myapp/questions_screen.dart';

class Quiz extends StatefulWidget {
  const Quiz({super.key});

  @override
  State<Quiz> createState() {
    return _QuizState();
  }
}

class _QuizState extends State<Quiz> {
  var activeScreen = 'start_screen';

  void updateScreen() {
    setState(() {
      activeScreen = "questions_screen";
    });
  }

  @override
  Widget build(BuildContext context) {
   
    final activeScreenWidget = activeScreen == 'start_screen'
        ? StartScreen(updateScreen)
        : const QuestionsScreen();

    return MaterialApp(
      home: Scaffold(
        body: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color.fromARGB(255, 71, 12, 180),
                Color.fromARGB(255, 227, 222, 240)
              ],
            ),
          ),
          child: activeScreenWidget,
        ),
      ),
    );
  }
}
