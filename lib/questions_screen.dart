import 'package:flutter/material.dart';

import 'package:myapp/data/questions.dart';
import 'package:myapp/answer_button.dart';

class QuestionsScreen extends StatelessWidget {
  const QuestionsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(questions[0].question,
              style: const TextStyle(color: Colors.white)),
          const SizedBox(height: 30),
          ...[...List.of(questions[0].answers)].map((answer) {
            return AnswerButton(onTap: () {}, answerText: answer);
          }),
        ],
      ),
    );
  }
}
